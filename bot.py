import json
import urllib.request
import sys
import os

def sendMessage(icon, channel, userId, message, url):
    color = ""
    payload = {"icon_url": icon,
           "channel": channel,
           "username": userId,
           "attachments": [{
               "color": color,
               "mrkdwn_in": ["pretext", "text", "fields"],
               "fields": [{
                   "short": False,
                   "value": message
               }],
               "fallback": message
    }]}

    encodedPayload = json.dumps(payload).encode('utf8')

    req = urllib.request.Request(url, data = encodedPayload, headers = {'Content-Type': 'application/json'})
    response = urllib.request.urlopen(req)
    responseContent = response.read()
    print("Mattermost responded with status code {}: {}".format(response.getcode(), responseContent))

with open('config.json') as configFile:
    config = json.load(configFile)
    message = sys.argv[1]
    sendMessage(config['icon'], config['channel'], config['userId'], message, config['hookUrl'])
