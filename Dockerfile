FROM python:3

ENV TZ=Europe/Berlin

RUN apt-get update && apt-get -y install cron vim

WORKDIR /root
COPY bot.py .
COPY init.sh .
COPY crontab /etc/cron.d/bot-cron
RUN crontab /etc/cron.d/bot-cron

ENTRYPOINT /bin/bash init.sh
