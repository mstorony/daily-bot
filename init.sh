#!/bin/bash

if [ ! -f config.json ]; then
    echo "Config file does not exist, make one?"
    select yn in "Yes" "No"; do
    case $yn in
        Yes ) vim config.json; break;;
        No ) exit;;
    esac
    done
fi

echo "Bot started"
cron -f
