### Daily scrum bot

See messages and when they are configured in crontab. When started, init.sh will prompt for a config file. This can be copied and modified from the config.json file.

# Running
``` 
docker run --name=daily-bot -it gitlab-registry.cern.ch/mstorony/daily-bot
``` 

First time the bot it started, it will ask for a config. See config.json for an example. After doing this, detach from the container. Then, start the container again by running:

```
docker start daily-bot
```

# Building and pushing
```
docker login gitlab-registry.cern.ch
docker build -t gitlab-registry.cern.ch/mstorony/daily-bot .
docker push gitlab-registry.cern.ch/mstorony/daily-bot
```